Source: nanobind
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 dh-sequence-python3,
 cmake,
 libeigen3-dev <!nocheck>,
 libpython3-all-dev,
 python3-all-dev:any,
 python3-numpy <!nocheck>,
 python3-pytest <!nocheck>,
 python3-scipy <!nocheck>,
 python3-setuptools,
 robin-map-dev <!nocheck>,
Homepage: https://github.com/wjakob/nanobind
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/nanobind.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/nanobind
Description: Tiny and efficient C++/Python bindings
 nanobind is a small binding library that exposes C++ types in Python and vice
 versa. It is reminiscent of Boost.Python and pybind11 and uses near-identical
 syntax. In contrast to these existing tools, nanobind is more efficient:
 bindings compile in a shorter amount of time, produce smaller binaries, and
 have better runtime performance.

Package: nanobind-dev
Architecture: all
Multi-Arch: foreign
Depends:
 libeigen3-dev,
 robin-map-dev,
 ${misc:Depends},
Description: ${source:Synopsis} (development files)
 ${source:Extended-Description}
 .
 This package installs the development files.

Package: python3-nanobind
Architecture: all
Depends:
 nanobind-dev (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
Provides:
 ${nanobind:Provides},
Description: ${source:Synopsis} (Python 3 module)
 ${source:Extended-Description}
 .
 This package installs the Python 3 module.
